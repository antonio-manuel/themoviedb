In order to make the requests work properly you need to add a a file called apikey.gradle to the
buildsystem folder with this content:

ext {
    apiKey = "<YOUR_API_KEY>"
} 

Next Steps:
* Get API conf remotely: images urls and sizes, i.e.
* Improve shared elements, problems after rotation in detail and coming back.
* Add new architecture components to improve lifecycle handling
* Store some results locally (Room) to improve offline use
* Migrate to kotlin