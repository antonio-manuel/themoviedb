# This is a configuration file for ProGuard.
# http://proguard.sourceforge.net/index.html#manual/usage.html

-dontusemixedcaseclassnames

# Optimization is turned off by default. Dex does not like code run
# through the ProGuard optimize and preverify steps (and performs some
# of these optimizations on its own).
-dontpreverify

-dontoptimize

## START FABRIC ##
# https://docs.fabric.io/android/crashlytics/dex-and-proguard.html
-keepattributes *Annotation*
-keepattributes SourceFile,LineNumberTable
-keep public class * extends java.lang.Exception
## END FABRIC ##

# The support library contains references to newer platform versions.
# Don't warn about those in case this app is linking against an older
# platform version.  We know about them, and they are safe.
-dontwarn android.support.**

# Guava ProGuard (Sugar ORM depends on Guava)
-dontwarn javax.annotation.**
-dontwarn javax.inject.**
-dontwarn sun.misc.Unsafe

-dontwarn okio.**
-dontwarn retrofit2.Platform$Java8
-dontwarn com.google.**
-dontwarn com.squareup.okhttp.**
-dontwarn org.checkerframework.**
-dontwarn afu.org.checkerframework.**

-keep class * implements android.os.Parcelable {
     public static final android.os.Parcelable$Creator *;
}
-keepclassmembers class * implements android.os.Parcelable {
    static ** CREATOR;
}

# Just to hace clear logs.
# Could be necessary comment to get info in case of error
-dontnote com.google.**
-dontnote com.squareup.**
-dontnote retrofit2.**
-dontnote okhttp3.**
-dontnote io.reactivex.**
-dontnote org.apache.http.**
-dontnote android.net.http.**

