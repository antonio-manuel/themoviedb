package eu.antoniolopez.datamodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MoviesPageDto {

    @SerializedName(value = "page") private int page;
    @SerializedName(value = "results") private List<MovieDto> movies;
    @SerializedName(value = "total_pages") private int totalPages;
    @SerializedName(value = "total_results") private int totalResults;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public List<MovieDto> getMovies() {
        return movies;
    }

    public void setMovies(List<MovieDto> movies) {
        this.movies = movies;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }
}
