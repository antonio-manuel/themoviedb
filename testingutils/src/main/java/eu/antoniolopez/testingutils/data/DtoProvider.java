package eu.antoniolopez.testingutils.data;

import java.util.ArrayList;
import java.util.List;

import eu.antoniolopez.datamodel.MovieDto;
import eu.antoniolopez.datamodel.MoviesPageDto;

public class DtoProvider {
    public static final int LIST_SIZE = 20;

    public static MovieDto getMovieDto(int id) {
        MovieDto movieDto = new MovieDto();
        movieDto.setId(id);
        movieDto.setPosterPath("/jIhL6mlT7AblhbHJgEoiBIOUVl1.jpg");
        movieDto.setBackdropPath("mUkuc2wyV9dHLG0D0Loaw5pO2s8.jpg");
        movieDto.setVoteAverage(5.5);
        movieDto.setOverview("Seven noble families fight for control of the mythical land of Westeros.");
        movieDto.setFirstAirDate("1990-01-20");
        movieDto.setOriginalLanguage("es");
        movieDto.setOriginalName("Original name " + id);
        return movieDto;
    }

    public static List<MovieDto> getListOfMovies() {
        List<MovieDto> list = new ArrayList<>();
        for (int id = 0; id < LIST_SIZE; id++) {
            list.add(getMovieDto(id));
        }
        return list;
    }

    public static MoviesPageDto getMoviesPageDto() {
        MoviesPageDto moviesPageDto = new MoviesPageDto();
        moviesPageDto.setPage(1);
        moviesPageDto.setTotalPages(15);
        moviesPageDto.setTotalResults(300);
        moviesPageDto.setMovies(getListOfMovies());
        return moviesPageDto;
    }
}
