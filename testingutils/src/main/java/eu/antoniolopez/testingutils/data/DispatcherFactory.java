package eu.antoniolopez.testingutils.data;

import android.content.res.AssetManager;
import android.util.Log;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import okhttp3.mockwebserver.Dispatcher;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.RecordedRequest;

public class DispatcherFactory {

    private static final String TEST_ASSETS = "/src/test/assets/";

    public static Dispatcher provideDispacther() {
        return new Dispatcher() {
            @Override
            public MockResponse dispatch(RecordedRequest request) throws InterruptedException {
                try {
                    String fileName = getFileName(request.getMethod(), request.getPath());
                    if (!fileName.equals("")) {
                        int responseCode = Integer.valueOf(fileName.split("_")[1]);
                        return new MockResponse()
                                .setResponseCode(responseCode)
                                .setBody(readFileAsString(System.getProperty("user.dir") + TEST_ASSETS + fileName));
                    }

                } catch (Exception e) {
                    return new MockResponse().setResponseCode(500);
                }

                return new MockResponse().setResponseCode(404);
            }
        };
    }

    private static String readFileAsString(String filePath) throws IOException {
        StringBuilder fileData = new StringBuilder();
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead;
        while ((numRead = reader.read(buf)) != -1) {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
        return fileData.toString();
    }

    private static String readFileAsString(AssetManager assetManager, String fileName) throws IOException {
        BufferedReader reader = null;
        StringBuilder fileData = new StringBuilder();
        try {
            reader = new BufferedReader(
                    new InputStreamReader(assetManager.open(fileName)));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                fileData.append(mLine);
            }
        } catch (IOException e) {
            //log the exception
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    //log the exception
                }
            }
        }
        return fileData.toString();
    }

    private static String getFileName(String method, String path) {
        String fileName = "";
        if (path.contains("/popular")) {
            if(path.contains("-1")){
                fileName = "noresource_404_error_response.json";
            }else{
                fileName = "popular_200_ok_response.json";
            }
        }
        if (path.contains("/similar")) {
            if(path.contains("-1")){
                fileName = "noresource_404_error_response.json";
            }else{
                fileName = "similar_200_ok_response.json";
            }
        }
        if (fileName.equals("")) {
            Log.e(DispatcherFactory.class.getSimpleName(), "Missing call: " + method + " - " + path);
            String error = "error";
        }
        return fileName;
    }
}
