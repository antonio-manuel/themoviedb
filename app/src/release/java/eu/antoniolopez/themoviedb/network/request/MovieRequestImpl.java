package eu.antoniolopez.themoviedb.network.request;

import eu.antoniolopez.datamodel.MoviesPageDto;
import eu.antoniolopez.themoviedb.network.service.MovieApiService;
import io.reactivex.Observable;

public class MovieRequestImpl extends MovieRequest {

    private final MovieApiService movieApiService;

    public MovieRequestImpl(MovieApiService movieApiService) {
        this.movieApiService = movieApiService;
    }

    @Override
    public Observable<MoviesPageDto> getPage(int page){
        return movieApiService.getPopularMovies(page);
    }

    @Override
    public Observable<MoviesPageDto> getSimilar(String id){
        return movieApiService.getSimilarMovies(id);
    }
}
