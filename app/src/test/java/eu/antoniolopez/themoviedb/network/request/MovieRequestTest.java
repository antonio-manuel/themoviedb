package eu.antoniolopez.themoviedb.network.request;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import eu.antoniolopez.datamodel.ErrorDto;
import eu.antoniolopez.datamodel.MoviesPageDto;
import eu.antoniolopez.testingutils.data.DispatcherFactory;
import eu.antoniolopez.themoviedb.di.ApiModule;
import eu.antoniolopez.themoviedb.network.Endpoint;
import eu.antoniolopez.themoviedb.network.exception.RetrofitException;
import eu.antoniolopez.themoviedb.network.service.MovieApiService;
import okhttp3.mockwebserver.MockWebServer;
import retrofit2.Retrofit;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.fail;


@RunWith(JUnit4.class)
public class MovieRequestTest {

    private final int POPULAR_FIRST_ID = 31917;
    private final int POPULAR_TOTAL_RESULTS = 20000;
    private final int POPULAR_TOTAL_PAGES = 1000;

    private final int SIMILAR_FIRST_ID = 64122;
    private final int SIMILAR_TOTAL_RESULTS = 185;
    private final int SIMILAR_TOTAL_PAGES = 10;

    private final int BAD_REQUEST_CODE = 34;

    private MockWebServer server;
    private DebugMovieRequestImpl movieRequest;

    @Before
    public void setUp() throws Exception {
        initServer();
        movieRequest = new DebugMovieRequestImpl(getApiService());
        DebugMovieRequestImpl.setServiceEnabled(true);
    }

    @After
    public void tearDown() throws Exception {
        if (server != null) {
            server.close();
        }
    }

    @Test
    public void testGetPageMappedToDto() {
        MoviesPageDto moviesPageDto = movieRequest.getPage(1).blockingFirst();
        assertThat(moviesPageDto.getMovies().size(), greaterThan(0));
        assertThat(moviesPageDto.getMovies().get(0).getId(), equalTo(POPULAR_FIRST_ID));
        assertThat(moviesPageDto.getTotalResults(), equalTo(POPULAR_TOTAL_RESULTS));
        assertThat(moviesPageDto.getTotalPages(), equalTo(POPULAR_TOTAL_PAGES));
    }

    @Test
    public void testErrorGettingPageBecauseOfBadRequest() {
        try {
            movieRequest.getPage(-1).doOnError(throwable -> {
                RetrofitException error = (RetrofitException) throwable;
                ErrorDto response = error.getErrorBodyAs(ErrorDto.class);
                assertThat(response.getStatusCode(), equalTo(BAD_REQUEST_CODE));
                return;
            }).blockingFirst();
        } catch (RetrofitException exception) {
            return;
        }
        fail("testErrorGettingPageBecauseOfBadRequest should not return a correct api response");
    }

    @Test
    public void testGetSimilarMovies() {
        MoviesPageDto moviesPageDto = movieRequest.getSimilar("1").blockingFirst();
        assertThat(moviesPageDto.getMovies().size(), greaterThan(0));
        assertThat(moviesPageDto.getMovies().get(0).getId(), equalTo(SIMILAR_FIRST_ID));
        assertThat(moviesPageDto.getTotalResults(), equalTo(SIMILAR_TOTAL_RESULTS));
        assertThat(moviesPageDto.getTotalPages(), equalTo(SIMILAR_TOTAL_PAGES));
    }

    @Test
    public void testErrorGettingSimilarBecauseOfBadRequest() {
        try {
            movieRequest.getSimilar("-1").doOnError(throwable -> {
                RetrofitException error = (RetrofitException) throwable;
                ErrorDto response = error.getErrorBodyAs(ErrorDto.class);
                assertThat(response.getStatusCode(), equalTo(BAD_REQUEST_CODE));
            }).blockingFirst();
        } catch (RetrofitException exception) {
            return;
        }
        fail("testErrorGettingSimilarBecauseOfBadRequest should not return a correct api response");
    }

    private void initServer() throws Exception {
        server = new MockWebServer();
        server.start();
        server.setDispatcher(DispatcherFactory.provideDispacther());
        Endpoint.setEndpoint("http://" + server.getHostName() + ":" + server.getPort() + "/");
    }

    private MovieApiService getApiService() {
        ApiModule apiModule = new ApiModule();
        Retrofit retrofit = apiModule.provideRestAdapter(apiModule.provideGsonConverterFactory(), apiModule.provideRxJava2CallAdapterFactory(), apiModule.provideOkHttpClient(apiModule.provideSetDefaultParamInterceptor()));
        return apiModule.provideApiService(retrofit);
    }
}