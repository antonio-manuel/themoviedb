package eu.antoniolopez.themoviedb.network.mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.text.SimpleDateFormat;
import java.util.List;

import eu.antoniolopez.datamodel.MovieDto;
import eu.antoniolopez.testingutils.data.DtoProvider;
import eu.antoniolopez.themoviedb.vo.Movie;

import static eu.antoniolopez.testingutils.data.DtoProvider.LIST_SIZE;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(JUnit4.class)
public class MoviesMapperTest {

    @Test
    public void checkMovieDtoToMovieMappingMatches() {
        MoviesMapper mapper = new MoviesMapper();
        MovieDto movieDto = DtoProvider.getMovieDto(1);
        Movie movie = mapper.transform(movieDto);

        verifyDtoAndVoHasTheSameInfo(movieDto, movie);
    }

    @Test
    public void checkListOfMoviesDtoToListOfMovieMappingMatches() {
        MoviesMapper mapper = new MoviesMapper();
        List<MovieDto> dtoList = DtoProvider.getListOfMovies();
        List<Movie> movieList = mapper.transform(dtoList);

        for (int i = 0; i < LIST_SIZE; i++) {
            verifyDtoAndVoHasTheSameInfo(dtoList.get(i), movieList.get(i));
        }
    }

    private void verifyDtoAndVoHasTheSameInfo(MovieDto movieDto, Movie movie) {
        assertThat(movie.getPosterPath(), equalTo(movieDto.getPosterPath()));
        assertThat(movie.getId(), equalTo(movieDto.getId()));
        assertThat(movie.getBackdropPath(), equalTo(movieDto.getBackdropPath()));
        assertThat(movie.getVoteAverage(), equalTo(movieDto.getVoteAverage()));
        assertThat(movie.getOverview(), equalTo(movieDto.getOverview()));
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        assertThat(formatter.format(movie.getFirstAirDate()), equalTo(movieDto.getFirstAirDate()));
        assertThat(movie.getOriginalLanguage(), equalTo(movieDto.getOriginalLanguage()));
        assertThat(movie.getOriginalName(), equalTo(movieDto.getOriginalName()));
    }
}