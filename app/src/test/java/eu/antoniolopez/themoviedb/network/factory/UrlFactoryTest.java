package eu.antoniolopez.themoviedb.network.factory;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;

import eu.antoniolopez.themoviedb.network.Endpoint;
import eu.antoniolopez.themoviedb.utils.TextUtils;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class UrlFactoryTest {

    @Test
    public void checkEmptyPosterStringReturnNullOrEmpty() {
        assertNull(UrlFactory.getPosterUrl(null));
        assertNull(UrlFactory.getPosterUrl(""));
        assertNull(UrlFactory.getPosterUrl(" "));
    }

    @Test
    public void checkPosterUrlContainsAllParts() {
        String poster = "end";
        String url = UrlFactory.getPosterUrl(poster);

        assertTrue(url.endsWith(poster));
        assertTrue(url.startsWith(Endpoint.POSTER_URL));
    }

    @Test
    public void checkEmptyBackDropStringReturnNullOrEmpty() {
        String poster = "end";
        String url = UrlFactory.getBackDropUrl(poster);

        assertTrue(url.endsWith(poster));
        assertTrue(url.startsWith(Endpoint.BACKDROP_URL));
    }
}