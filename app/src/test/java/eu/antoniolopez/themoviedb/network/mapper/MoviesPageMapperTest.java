package eu.antoniolopez.themoviedb.network.mapper;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import eu.antoniolopez.datamodel.MoviesPageDto;
import eu.antoniolopez.testingutils.data.DtoProvider;
import eu.antoniolopez.themoviedb.vo.MoviesPage;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

@RunWith(JUnit4.class)
public class MoviesPageMapperTest {

    @Test
    public void checkMoviesPageDtoToMoviesPageMappingMatches() {
        MoviesPageMapper mapper = new MoviesPageMapper(new MoviesMapper());
        MoviesPageDto moviesPageDto = DtoProvider.getMoviesPageDto();
        MoviesPage moviesPage = mapper.transform(moviesPageDto);

        assertThat(moviesPage.getPage(), equalTo(moviesPageDto.getPage()));
        assertThat(moviesPage.getTotalPages(), equalTo(moviesPageDto.getTotalPages()));
        assertThat(moviesPage.getTotalResults(), equalTo(moviesPageDto.getTotalResults()));
    }
}