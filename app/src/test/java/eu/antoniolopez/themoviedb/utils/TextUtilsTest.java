package eu.antoniolopez.themoviedb.utils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class TextUtilsTest {

    @Test
    public void checkTrueWhenStringNullOrSpaces() {
        assertTrue(TextUtils.isEmpty(null));
        assertTrue(TextUtils.isEmpty("   "));
    }

    @Test
    public void checkFalseIfAnyChar() {
        assertFalse(TextUtils.isEmpty("a"));
    }
}