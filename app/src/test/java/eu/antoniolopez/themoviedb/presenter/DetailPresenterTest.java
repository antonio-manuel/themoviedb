package eu.antoniolopez.themoviedb.presenter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import eu.antoniolopez.themoviedb.network.RequestError;
import eu.antoniolopez.themoviedb.network.mapper.ErrorMapper;
import eu.antoniolopez.themoviedb.ui.contract.MovieDetailContract;
import eu.antoniolopez.themoviedb.usecase.CheckConnectivity;
import eu.antoniolopez.themoviedb.usecase.GetSimilarMovies;
import eu.antoniolopez.themoviedb.vo.Movie;
import eu.antoniolopez.themoviedb.vo.MovieError;
import io.reactivex.Observable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class DetailPresenterTest {

    @Mock
    private GetSimilarMovies getSimilarMovies;
    @Mock
    private CheckConnectivity checkConnectivity;
    @Mock
    private ErrorMapper mapper;
    @Mock
    private MovieDetailContract view;
    @InjectMocks
    private DetailPresenter detailPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void checkOnMovieReceivedAndHasConnectionGetSimilarIsCalled() {
        when(checkConnectivity.isOnline()).thenReturn(Observable.just(true));
        int id = 1;

        detailPresenter.onMovieReceived(id);

        verify(getSimilarMovies).getSimilarMovies(String.valueOf(id));
    }


    @Test
    public void checkOnSuccessGetSimilarThenViewReceivesNewData() {
        List<Movie> list = new ArrayList<>();
        list.add(new Movie());

        detailPresenter.onSuccess(list);

        verify(view).showSimilarMovies(list);
    }

    @Test
    public void checkOnSimilarErrorThenViewShowIt() {
        MovieError movieError = new MovieError(RequestError.NO_DATA);

        detailPresenter.onError(movieError);

        verify(view).showError(movieError);
    }

    @Test
    public void checkOnFinishViewIsClosedAndUseCaseDisconnected() {
        detailPresenter.onFinished();

        verify(view).close();
    }
}