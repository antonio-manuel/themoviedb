package eu.antoniolopez.themoviedb.presenter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import eu.antoniolopez.themoviedb.navigation.Navigator;
import eu.antoniolopez.themoviedb.network.RequestError;
import eu.antoniolopez.themoviedb.network.mapper.ErrorMapper;
import eu.antoniolopez.themoviedb.ui.contract.MovieListContract;
import eu.antoniolopez.themoviedb.usecase.CheckConnectivity;
import eu.antoniolopez.themoviedb.usecase.GetPopularMovies;
import eu.antoniolopez.themoviedb.vo.Movie;
import eu.antoniolopez.themoviedb.vo.MovieError;
import io.reactivex.Observable;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class ListPresenterTest {

    @Mock
    private GetPopularMovies getPopularMovies;
    @Mock
    private CheckConnectivity checkConnectivity;
    @Mock
    private Navigator navigator;
    @Mock
    private ErrorMapper mapper;
    @Mock
    private MovieListContract view;
    @InjectMocks
    private ListPresenter listPresenter;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void onViewCreated() {
        when(checkConnectivity.isOnline()).thenReturn(Observable.just(true));

        listPresenter.onViewCreated();

        verify(view).showLoading();
        verify(getPopularMovies).getPage(1);
    }

    @Test
    public void onItemClick() {
        Movie movie = new Movie();
        Integer sample = 1;

        listPresenter.onItemClick(movie, sample);

        verify(navigator).goToDetail(movie, sample);
    }

    @Test
    public void updateWithConnectivity() {
        when(checkConnectivity.isOnline()).thenReturn(Observable.just(true));

        listPresenter.onViewCreated();

        verify(view).showLoading();
        verify(getPopularMovies).getPage(1);
    }

    @Test
    public void updateWithoutConnectivity() {
        when(checkConnectivity.isOnline()).thenReturn(Observable.just(false));

        listPresenter.onViewCreated();

        ArgumentCaptor<MovieError> error = ArgumentCaptor.forClass(MovieError.class);
        verify(view).showError(error.capture());
    }

    @Test
    public void onSuccess() {
        List<Movie> list = new ArrayList<>();
        list.add(new Movie());
        int page = 1;
        int pages = 10;

        listPresenter.onSuccess(list, page, pages);

        verify(view).updateData(list, page, pages);
        verify(view).hideLoading();
    }

    @Test
    public void onError() {
        MovieError movieError = new MovieError(RequestError.NO_DATA);

        listPresenter.onError(movieError);

        verify(view).showError(movieError);
        verify(view).hideLoading();
    }
}