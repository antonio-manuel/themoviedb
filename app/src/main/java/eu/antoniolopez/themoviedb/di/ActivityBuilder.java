package eu.antoniolopez.themoviedb.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import eu.antoniolopez.themoviedb.ui.activity.DetailActivity;
import eu.antoniolopez.themoviedb.ui.activity.ListActivity;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = {ListActivityModule.class})
    abstract ListActivity bindListActivity();

    @ContributesAndroidInjector(modules = {DetailActivityModule.class})
    abstract DetailActivity bindDetailActivity();

}
