package eu.antoniolopez.themoviedb.di;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import eu.antoniolopez.themoviedb.ui.activity.DetailActivity;
import eu.antoniolopez.themoviedb.ui.contract.MovieDetailContract;
import eu.antoniolopez.themoviedb.ui.fragment.DetailFragment;

@Module
public abstract class DetailActivityModule {

    @ContributesAndroidInjector
    abstract DetailFragment provideDetailFragmentFactory();

    @Binds
    abstract MovieDetailContract provideMovieDetailView(DetailActivity mainActivity);

    @Binds
    abstract AppCompatActivity activity(DetailActivity activity);

    @Provides
    public static FragmentManager provideFragmentManager(AppCompatActivity activity){
        return activity.getSupportFragmentManager();
    }

}

