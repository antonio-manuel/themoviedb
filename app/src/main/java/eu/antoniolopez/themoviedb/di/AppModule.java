package eu.antoniolopez.themoviedb.di;

import android.app.Application;
import android.content.Context;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import eu.antoniolopez.themoviedb.di.qualifier.ForObserveScheduler;
import eu.antoniolopez.themoviedb.di.qualifier.ForSubscribeScheduler;
import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

@Module(includes = {ApiModule.class, RequestModule.class})
public abstract class AppModule {

    @Binds
    abstract Context provideContext(Application application);

    @Provides
    @Singleton
    @ForSubscribeScheduler
    public static Scheduler provideSchedulerSubscribe() {
        return Schedulers.io();
    }

    @Provides
    @Singleton
    @ForObserveScheduler
    public static Scheduler provideSchedulerObserve() {
        return AndroidSchedulers.mainThread();
    }
}