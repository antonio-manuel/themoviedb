package eu.antoniolopez.themoviedb.di;

import com.google.gson.Gson;

import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import eu.antoniolopez.themoviedb.BuildConfig;
import eu.antoniolopez.themoviedb.network.Endpoint;
import eu.antoniolopez.themoviedb.network.factory.RxErrorHandlingCallAdapterFactory;
import eu.antoniolopez.themoviedb.network.service.MovieApiService;
import io.reactivex.schedulers.Schedulers;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    @Provides
    @Singleton
    public GsonConverterFactory provideGsonConverterFactory() {
        return GsonConverterFactory.create(new Gson());
    }

    @Provides
    @Singleton
    public CallAdapter.Factory provideRxJava2CallAdapterFactory() {
        return RxErrorHandlingCallAdapterFactory.create(Schedulers.io());
    }

    @Provides
    public Interceptor provideSetDefaultParamInterceptor() {
        return chain -> {
            Request original = chain.request();
            HttpUrl originalHttpUrl = original.url();

            String language;
            try {
                StringBuilder builder = new StringBuilder();
                builder.append(Locale.getDefault().getISO3Language());
                builder.append("-");
                builder.append(Locale.getDefault().getISO3Country());
                language = builder.toString();
            }catch (Exception e){
                language = Endpoint.DEFAULT_LOCALE;
            }
            HttpUrl url = originalHttpUrl.newBuilder()
                    .addQueryParameter(Endpoint.PARAM_API_KEY, BuildConfig.THE_MOVIE_API_KEY)
                    .addQueryParameter(Endpoint.PARAM_LANGUAGE, language)
                    .build();

            Request.Builder requestBuilder = original.newBuilder()
                    .url(url);

            Request request = requestBuilder.build();

            return chain.proceed(request);
        };
    }

    @Provides
    @Singleton
    public OkHttpClient provideOkHttpClient(Interceptor interceptor) {
        OkHttpClient.Builder b = new OkHttpClient.Builder();
        b.connectTimeout(10, TimeUnit.SECONDS);
        b.readTimeout(10, TimeUnit.SECONDS);
        b.writeTimeout(10, TimeUnit.SECONDS);

        b.addInterceptor(interceptor);

        return b.build();
    }

    @Provides
    @Singleton
    public Retrofit provideRestAdapter(GsonConverterFactory gsonConverterFactory, CallAdapter.Factory rxJava2CallAdapterFactory, OkHttpClient okHttpClient) {
        return new Retrofit.Builder()
                .baseUrl(Endpoint.getEndpoint())
                .addCallAdapterFactory(rxJava2CallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .client(okHttpClient)
                .build();
    }

    @Provides
    @Singleton
    public MovieApiService provideApiService(Retrofit retrofit) {
        return retrofit.create(MovieApiService.class);
    }

}
