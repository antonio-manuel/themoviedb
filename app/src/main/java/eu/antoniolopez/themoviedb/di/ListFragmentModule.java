package eu.antoniolopez.themoviedb.di;

import dagger.Module;
import dagger.Provides;
import eu.antoniolopez.themoviedb.ui.contract.MovieListContract;
import eu.antoniolopez.themoviedb.ui.fragment.ListFragment;

@Module
public class ListFragmentModule {

    @Provides
    static MovieListContract provideMovieListView(ListFragment listFragment){
        return listFragment;
    }
}
