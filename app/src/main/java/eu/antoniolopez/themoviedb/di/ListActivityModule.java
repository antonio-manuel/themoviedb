package eu.antoniolopez.themoviedb.di;

import android.support.v7.app.AppCompatActivity;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import eu.antoniolopez.themoviedb.ui.activity.ListActivity;
import eu.antoniolopez.themoviedb.ui.fragment.ListFragment;

@Module
public abstract class ListActivityModule {

    @ContributesAndroidInjector(modules = ListFragmentModule.class)
    abstract ListFragment provideListFragmentFactory();

    @Binds
    abstract AppCompatActivity activity(ListActivity activity);

}
