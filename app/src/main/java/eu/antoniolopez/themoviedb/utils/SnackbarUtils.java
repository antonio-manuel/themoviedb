package eu.antoniolopez.themoviedb.utils;

import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.TextView;

public class SnackbarUtils {

    public static void showLongSnackBar(View view, String text, int duration) {
        Snackbar snackbar = Snackbar.make(view, text, duration);
        View snackbarView = snackbar.getView();
        ((TextView) snackbarView.findViewById(android.support.design.R.id.snackbar_text)).setMaxLines(5);
        snackbar.show();
    }
}
