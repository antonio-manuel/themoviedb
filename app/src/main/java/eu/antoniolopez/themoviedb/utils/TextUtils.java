package eu.antoniolopez.themoviedb.utils;

public class TextUtils {
    public static boolean isEmpty(String text) {
        return text == null || text.trim().equals("");
    }
}
