package eu.antoniolopez.themoviedb;

import android.content.Context;
import android.support.multidex.MultiDex;

import dagger.android.AndroidInjector;
import dagger.android.DaggerApplication;
import eu.antoniolopez.themoviedb.di.AppComponent;
import eu.antoniolopez.themoviedb.di.DaggerAppComponent;

public class CustomApp extends DaggerApplication {

    protected AndroidInjector<? extends DaggerApplication> applicationInjector() {
        AppComponent appComponent = DaggerAppComponent.builder().application(this).build();
        appComponent.inject(this);
        return appComponent;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        if(BuildConfig.DEBUG){
            MultiDex.install(context);
        }
    }
}
