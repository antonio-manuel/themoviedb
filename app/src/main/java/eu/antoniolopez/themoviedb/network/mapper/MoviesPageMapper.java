package eu.antoniolopez.themoviedb.network.mapper;

import javax.inject.Inject;

import eu.antoniolopez.datamodel.MoviesPageDto;
import eu.antoniolopez.themoviedb.vo.MoviesPage;

public class MoviesPageMapper {

    private final MoviesMapper moviesMapper;

    @Inject
    public MoviesPageMapper(MoviesMapper moviesMapper) {
        this.moviesMapper = moviesMapper;
    }

    public MoviesPage transform(MoviesPageDto moviesPageDto) {
        if (moviesPageDto == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        final MoviesPage moviesPage = new MoviesPage();
        moviesPage.setPage(moviesPageDto.getPage());
        moviesPage.setTotalPages(moviesPageDto.getTotalPages());
        moviesPage.setTotalResults(moviesPageDto.getTotalResults());
        moviesPage.setMovies(moviesMapper.transform(moviesPageDto.getMovies()));
        return moviesPage;
    }
}
