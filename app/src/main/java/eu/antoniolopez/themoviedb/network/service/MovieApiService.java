package eu.antoniolopez.themoviedb.network.service;

import eu.antoniolopez.datamodel.MoviesPageDto;
import eu.antoniolopez.themoviedb.network.Endpoint;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface MovieApiService {

    @GET(Endpoint.ENDPOINT_POPULAR)
    Observable<MoviesPageDto> getPopularMovies(
            @Query(Endpoint.PARAM_PAGE) int page);

    @GET(Endpoint.ENDPOINT_SIMILAR)
    Observable<MoviesPageDto> getSimilarMovies(
            @Path(Endpoint.PARAM_MOVIE_ID) String movieId);
}
