package eu.antoniolopez.themoviedb.network.request;

import eu.antoniolopez.datamodel.MoviesPageDto;
import io.reactivex.Observable;

public abstract class MovieRequest {

    public abstract Observable<MoviesPageDto> getPage(int page);

    public abstract Observable<MoviesPageDto> getSimilar(String id);
}
