package eu.antoniolopez.themoviedb.network.mapper;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.inject.Inject;

import eu.antoniolopez.datamodel.MovieDto;
import eu.antoniolopez.themoviedb.vo.Movie;

public class MoviesMapper {

    @Inject
    public MoviesMapper() {

    }

    public Movie transform(MovieDto movieDto) {
        if (movieDto == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        final Movie movie = new Movie();
        movie.setPosterPath(movieDto.getPosterPath());
        movie.setId(movieDto.getId());
        movie.setBackdropPath(movieDto.getBackdropPath());
        movie.setVoteAverage(movieDto.getVoteAverage());
        movie.setOverview(movieDto.getOverview());
        try{
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            movie.setFirstAirDate(format.parse(movieDto.getFirstAirDate()));
        }catch (Exception e){
            movie.setFirstAirDate(null);
        }
        movie.setOriginalLanguage(movieDto.getOriginalLanguage());
        movie.setOriginalName(movieDto.getOriginalName());
        return movie;
    }

    public List<Movie> transform(List<MovieDto> movieDtoCollection) {
        List<Movie> movieCollection;

        if (movieDtoCollection != null && !movieDtoCollection.isEmpty()) {
            movieCollection = new ArrayList<>();
            for (MovieDto movieDto : movieDtoCollection) {
                movieCollection.add(transform(movieDto));
            }
        } else {
            movieCollection = Collections.emptyList();
        }

        return movieCollection;
    }
}
