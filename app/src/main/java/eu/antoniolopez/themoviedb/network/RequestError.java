package eu.antoniolopez.themoviedb.network;

public enum RequestError {
    NO_DATA,
    REQUEST_ERROR,
    NO_CONNECTION
}
