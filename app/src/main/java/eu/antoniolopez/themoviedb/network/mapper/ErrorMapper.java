package eu.antoniolopez.themoviedb.network.mapper;

import java.io.IOException;

import javax.inject.Inject;

import eu.antoniolopez.datamodel.ErrorDto;
import eu.antoniolopez.themoviedb.network.RequestError;
import eu.antoniolopez.themoviedb.network.exception.RetrofitException;
import eu.antoniolopez.themoviedb.vo.MovieError;

public class ErrorMapper {

    @Inject
    public ErrorMapper() {

    }

    public MovieError transform(ErrorDto errorDto) {
        final MovieError movieError = new MovieError(RequestError.REQUEST_ERROR);
        if (errorDto != null) {
            movieError.setStatusMessage(errorDto.getStatusMessage());
            movieError.setStatusCode(errorDto.getStatusCode());
            movieError.setSuccess(errorDto.isSuccess());
        } else {
            movieError.setRequestError(RequestError.NO_CONNECTION);
        }
        return movieError;
    }

    public MovieError transform(Throwable throwable) {
        if (throwable == null) {
            throw new IllegalArgumentException("Cannot transform a null value");
        }
        ErrorDto errorDto;
        RetrofitException error = (RetrofitException) throwable;
        try {
            errorDto = error.getErrorBodyAs(ErrorDto.class);
        } catch (IOException e) {
            errorDto = null;
        }
        return transform(errorDto);
    }
}
