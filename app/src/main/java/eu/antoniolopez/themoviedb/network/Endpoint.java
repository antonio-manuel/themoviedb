package eu.antoniolopez.themoviedb.network;

public class Endpoint {

    private static final String THE_MOVIE_API_BASEPATH = "http://api.themoviedb.org/3/";

    public static final String ENDPOINT_POPULAR = "tv/popular";
    public static final String ENDPOINT_SIMILAR = "tv/{movie_id}/similar";

    public static final String PARAM_API_KEY = "api_key";
    public static final String PARAM_PAGE = "page";
    public static final String PARAM_MOVIE_ID = "movie_id";
    public static final String PARAM_LANGUAGE = "language";

    public static final String DEFAULT_LOCALE = "en_US";

    public static final String IMAGE_URL = "http://image.tmdb.org/t/p/";
    public static final String POSTER_URL = IMAGE_URL + "w185/";
    public static final String BACKDROP_URL = IMAGE_URL + "w780/";

    private static String endpoint;

    public static String getEndpoint() {
        return endpoint != null ? endpoint : THE_MOVIE_API_BASEPATH;
    }

    public static void setEndpoint(String endpoint) {
        Endpoint.endpoint = endpoint;
    }
}
