package eu.antoniolopez.themoviedb.network.factory;


import eu.antoniolopez.themoviedb.network.Endpoint;
import eu.antoniolopez.themoviedb.utils.TextUtils;

public class UrlFactory {

    public static String getPosterUrl(String part) {
        if (TextUtils.isEmpty(part)) {
            return null;
        } else {
            return Endpoint.POSTER_URL + part;
        }
    }

    public static String getBackDropUrl(String part) {
        if (TextUtils.isEmpty(part)) {
            return null;
        } else {
            return Endpoint.BACKDROP_URL + part;
        }
    }
}
