package eu.antoniolopez.themoviedb.presenter;

import java.util.List;

import javax.inject.Inject;

import eu.antoniolopez.themoviedb.navigation.Navigator;
import eu.antoniolopez.themoviedb.network.RequestError;
import eu.antoniolopez.themoviedb.network.mapper.ErrorMapper;
import eu.antoniolopez.themoviedb.ui.contract.MovieListContract;
import eu.antoniolopez.themoviedb.usecase.CheckConnectivity;
import eu.antoniolopez.themoviedb.usecase.GetPopularMovies;
import eu.antoniolopez.themoviedb.vo.Movie;
import eu.antoniolopez.themoviedb.vo.MovieError;
import eu.antoniolopez.themoviedb.vo.MoviesPage;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class ListPresenter {

    private static final int PREFETCH_ROW = 2;

    private final GetPopularMovies getPopularMovies;
    private final CheckConnectivity checkConnectivity;
    private final Navigator navigator;
    private final ErrorMapper mapper;
    private final MovieListContract view;

    protected CompositeDisposable subscriptions;

    private final Object lock = new Object();
    private boolean isWaiting = false;
    private boolean refreshPressed = false;
    private int page = 0;

    private Consumer<Boolean> connectivityConsumer = networkAvailable -> {
        if (networkAvailable) {
            synchronized (lock) {
                ListPresenter.this.view.showLoading();
                isWaiting = true;
            }
        }
    };

    private Consumer<MoviesPage> resultConsumer = moviesPage -> {
        if (moviesPage.getMovies() != null && moviesPage.getMovies().size() > 0) {
            onSuccess(moviesPage.getMovies(), moviesPage.getPage(), moviesPage.getTotalPages());
        } else {
            onError(new MovieError(RequestError.NO_DATA));
        }
    };

    private Consumer<Throwable> errorConsumer = throwable -> {
        onError(ListPresenter.this.mapper.transform(throwable));
    };

    @Inject
    public ListPresenter(GetPopularMovies getPopularMovies, CheckConnectivity checkConnectivity, Navigator navigator, ErrorMapper mapper, MovieListContract view) {
        this.getPopularMovies = getPopularMovies;
        this.checkConnectivity = checkConnectivity;
        this.navigator = navigator;
        this.mapper = mapper;
        this.view = view;
        this.subscriptions = new CompositeDisposable();
    }

    public void onViewCreated() {
        updateMovies(1);
    }

    public void onScroll(int visibleItemCount, int pastVisiblesItems, int totalItemCount, int page, int maxPage, int itemPerRow) {
        if ((visibleItemCount + pastVisiblesItems + itemPerRow * PREFETCH_ROW) >= totalItemCount && (page < maxPage || maxPage == 0)) {
            updateMovies(page + 1);
        }
    }

    private void updateMovies(int nextPage) {
        if (!isWaiting) {
            subscriptions.add(checkConnectivity.isOnline()
                    .doOnNext(connectivityConsumer)
                    .filter(isNetworkAvailable -> true)
                    .flatMap(isAvailable -> getPopularMovies.getPage(nextPage))
                    .subscribe(resultConsumer, errorConsumer));
        }
    }

    public void onRefresh() {
        refreshPressed = true;
        updateMovies(1);
    }

    public void onItemClick(Movie movie, Object transition) {
        navigator.goToDetail(movie, transition);
    }

    void onSuccess(List<Movie> movieList, int page, int totalPages) {
        synchronized (lock) {
            isWaiting = false;
            if (this.page < page || refreshPressed) {
                this.page = page;
                view.updateData(movieList, page, totalPages);
            }
        }
        view.hideLoading();
    }

    void onError(MovieError error) {
        synchronized (lock) {
            isWaiting = false;
        }
        view.hideLoading();
        view.showError(error);
    }

    public void onDestroy() {
        subscriptions.dispose();
    }
}
