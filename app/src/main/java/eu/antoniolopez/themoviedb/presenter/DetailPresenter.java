package eu.antoniolopez.themoviedb.presenter;

import java.util.List;

import javax.inject.Inject;

import eu.antoniolopez.themoviedb.network.RequestError;
import eu.antoniolopez.themoviedb.network.mapper.ErrorMapper;
import eu.antoniolopez.themoviedb.ui.contract.MovieDetailContract;
import eu.antoniolopez.themoviedb.usecase.CheckConnectivity;
import eu.antoniolopez.themoviedb.usecase.GetSimilarMovies;
import eu.antoniolopez.themoviedb.vo.Movie;
import eu.antoniolopez.themoviedb.vo.MovieError;
import eu.antoniolopez.themoviedb.vo.MoviesPage;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Consumer;

public class DetailPresenter {

    private final GetSimilarMovies getSimilarMovies;
    private final CheckConnectivity checkConnectivity;
    private final ErrorMapper mapper;
    private final MovieDetailContract view;

    protected CompositeDisposable subscriptions;

    private Consumer<MoviesPage> resultConsumer = moviesPage -> {
        if (moviesPage.getMovies() != null && moviesPage.getMovies().size() > 0) {
            onSuccess(moviesPage.getMovies());
        } else {
            onError(new MovieError(RequestError.NO_DATA));
        }
    };

    private Consumer<Throwable> errorConsumer = throwable -> {
        onError(DetailPresenter.this.mapper.transform(throwable));
    };

    @Inject
    public DetailPresenter(GetSimilarMovies getSimilarMovies, CheckConnectivity checkConnectivity, ErrorMapper mapper, MovieDetailContract view) {
        this.getSimilarMovies = getSimilarMovies;
        this.checkConnectivity = checkConnectivity;
        this.mapper = mapper;
        this.view = view;
        this.subscriptions = new CompositeDisposable();
    }

    public void onMovieReceived(int id) {
        subscriptions.add(checkConnectivity.isOnline()
                .filter(isNetworkAvailable -> true)
                .flatMap(isAvailable -> getSimilarMovies.getSimilarMovies(String.valueOf(id)))
                .subscribe(resultConsumer, errorConsumer));
    }

    public void onDestroy() {
        subscriptions.dispose();
    }

    void onSuccess(List<Movie> movieList) {
        view.showSimilarMovies(movieList);
    }

    void onError(MovieError movieError) {
        view.showError(movieError);
    }

    public void onFinished() {
        view.close();
        subscriptions.dispose();
    }
}
