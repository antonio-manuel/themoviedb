package eu.antoniolopez.themoviedb.navigation;

import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.ViewCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import javax.inject.Inject;

import eu.antoniolopez.themoviedb.ui.activity.DetailActivity;
import eu.antoniolopez.themoviedb.vo.Movie;

public class Navigator {

    private final AppCompatActivity activity;

    @Inject
    public Navigator(AppCompatActivity activity) {
        this.activity = activity;
    }

    public void goToDetail(Movie movie, Object transition) {
        ImageView image = transition instanceof ImageView ? (ImageView) transition : null;
        Intent intent = DetailActivity.newInstance(activity, movie);

        ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                activity,
                image,
                ViewCompat.getTransitionName(image));
        activity.startActivity(intent, options.toBundle());
    }
}
