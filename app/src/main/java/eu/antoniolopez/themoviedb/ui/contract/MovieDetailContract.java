package eu.antoniolopez.themoviedb.ui.contract;

import java.util.List;

import eu.antoniolopez.themoviedb.network.RequestError;
import eu.antoniolopez.themoviedb.vo.Movie;
import eu.antoniolopez.themoviedb.vo.MovieError;

public interface MovieDetailContract {
    void showSimilarMovies(List<Movie> movieList);

    void showError(MovieError movieError);

    void close();
}
