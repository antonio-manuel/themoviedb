package eu.antoniolopez.themoviedb.ui.fragment;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.antoniolopez.themoviedb.R;
import eu.antoniolopez.themoviedb.presenter.ListPresenter;
import eu.antoniolopez.themoviedb.ui.adapter.MovieListAdapter;
import eu.antoniolopez.themoviedb.ui.contract.MovieListContract;
import eu.antoniolopez.themoviedb.utils.SnackbarUtils;
import eu.antoniolopez.themoviedb.utils.TextUtils;
import eu.antoniolopez.themoviedb.vo.Movie;
import eu.antoniolopez.themoviedb.vo.MovieError;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

public class ListFragment extends BaseFragment implements MovieListContract, MovieListAdapter.OnItemClickListener {

    private static final String PAGE = "current_page";
    private static final String MAX_PAGE = "max_pages";

    View rootView;
    @BindView(R.id.empty_view)
    View emptyView;
    @BindView(R.id.list_container)
    RecyclerView recyclerView;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    GridLayoutManager gridLayoutManager;
    RecyclerView.OnScrollListener endlessRecyclerOnScrollListener;

    @Inject ListPresenter presenter;
    @Inject MovieListAdapter adapter;

    private int page = 0;
    private int maxPage = 0;
    private ArrayList<Movie> data = new ArrayList<>();

    public static Fragment newInstance() {
        return new ListFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setRetainInstance(true);
        rootView = inflater.inflate(R.layout.fragment_list, container, false);
        ButterKnife.bind(this, rootView);
        initRecyclerView();
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (savedInstanceState != null) {
            data = savedInstanceState.getParcelableArrayList(data.getClass().getCanonicalName());
            page = savedInstanceState.getInt(PAGE);
            maxPage = savedInstanceState.getInt(MAX_PAGE);
        } else {
            if (data.size() == 0) {
                presenter.onViewCreated();
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(data.getClass().getCanonicalName(), data);
        outState.putInt(PAGE, page);
        outState.putInt(MAX_PAGE, maxPage);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        presenter.onDestroy();
    }

    private void initRecyclerView() {
        gridLayoutManager = new GridLayoutManager(getActivity(), getResources().getInteger(R.integer.columns));
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);
        adapter.setOnItemClickListener(this);
        adapter.setMovieList(data);
        endlessRecyclerOnScrollListener = new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                int visibleItemCount = gridLayoutManager.getChildCount();
                int totalItemCount = gridLayoutManager.getItemCount();
                int pastVisiblesItems = gridLayoutManager.findFirstVisibleItemPosition();
                presenter.onScroll(visibleItemCount, pastVisiblesItems, totalItemCount, page, maxPage, getResources().getInteger(R.integer.columns));
            }
        };
        recyclerView.setOnScrollListener(endlessRecyclerOnScrollListener);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                presenter.onRefresh();
            }
        });
    }

    @Override
    public void showLoading() {
        swipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void updateData(List<Movie> movieList, int page, int totalPages) {
        boolean restartData = this.page > page;
        if (restartData) {
            data.clear();
        }
        this.page = page;
        data.addAll(movieList);
        this.maxPage = totalPages;
        if (restartData) {
            adapter.notifyDataSetChanged();
        } else {
            int start = data.indexOf(movieList.get(0));
            adapter.notifyItemRangeInserted(start, start + movieList.size() - 1);
        }

        if (recyclerView.getVisibility() == GONE) {
            emptyView.setVisibility(GONE);
            recyclerView.setVisibility(VISIBLE);
        }
    }

    @Override
    public void showError(MovieError movieError) {
        int errorId = 0;
        switch (movieError.getRequestError()) {
            case NO_DATA:
                errorId = R.string.error_data;
                break;
            case NO_CONNECTION:
                errorId = R.string.error_data_connection;
                break;
            case REQUEST_ERROR:
                errorId = R.string.error_response;
                break;
        }
        if (errorId > 0) {
            String text;
            if (!TextUtils.isEmpty(movieError.getStatusMessage())) {
                text = getString(R.string.composed_error, getString(errorId), movieError.getStatusMessage());
            } else {
                text = getString(errorId);
            }
            SnackbarUtils.showLongSnackBar(getActivity().findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG);
            if (data.size() == 0) {
                showEmptyList();
            }
        }
    }

    @Override
    public void onMovieItemClicked(Movie movie, ImageView image) {
        presenter.onItemClick(movie, image);
    }

    public void showEmptyList() {
        emptyView.setVisibility(VISIBLE);
        recyclerView.setVisibility(GONE);
    }
}
