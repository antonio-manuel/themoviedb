package eu.antoniolopez.themoviedb.ui.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import eu.antoniolopez.themoviedb.ui.fragment.DetailFragment;
import eu.antoniolopez.themoviedb.vo.Movie;

public class SimilarMoviesAdapter extends FragmentStatePagerAdapter implements ViewPager.OnPageChangeListener {

    private static final String STATE = "state";
    private static final String POSITION = "position";
    private ArrayList<Movie> data = new ArrayList<>();
    private OnFinishedListener listener;
    private int state;
    private int position;
    private String imageName;

    @Inject
    public SimilarMoviesAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        this.position = position;
        if(position == 0){
            return DetailFragment.newInstance(data.get(position), imageName);
        }else {
            return DetailFragment.newInstance(data.get(position));
        }
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        if (data.size() > 0) {
            return data.get(position).getOriginalName();
        } else {
            return null;
        }
    }

    public void setListener(OnFinishedListener listener) {
        this.listener = listener;
    }

    public void setMovie(Movie movie) {
        data.clear();
        data.add(movie);
        this.imageName = String.valueOf(movie.getId());
    }

    public void setList(List<Movie> movieList) {
        this.data.addAll(movieList);
        notifyDataSetChanged();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        if (state == 1 && (position + 1) == data.size() && positionOffset == 0.0 && listener != null) {
            listener.onScrollFinished();
        }
    }

    @Override
    public void onPageSelected(int position) {
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        this.state = state;
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(data.getClass().getCanonicalName(), data);
        outState.putInt(STATE, state);
        outState.putInt(POSITION, position);
    }

    public void onRestoreInstanceState(Bundle savedInstanceState) {
        data = savedInstanceState.getParcelableArrayList(data.getClass().getCanonicalName());
        state = savedInstanceState.getInt(STATE);
        position = savedInstanceState.getInt(POSITION);

    }

    public interface OnFinishedListener {
        void onScrollFinished();
    }
}
