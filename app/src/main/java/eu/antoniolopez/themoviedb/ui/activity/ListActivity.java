package eu.antoniolopez.themoviedb.ui.activity;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import eu.antoniolopez.themoviedb.R;
import eu.antoniolopez.themoviedb.ui.fragment.ListFragment;

public class ListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (savedInstanceState == null) {
            addFragment(R.id.container, ListFragment.newInstance());
        }
    }

}
