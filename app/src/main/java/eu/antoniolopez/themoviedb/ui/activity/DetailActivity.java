package eu.antoniolopez.themoviedb.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.MenuItem;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.antoniolopez.themoviedb.R;
import eu.antoniolopez.themoviedb.presenter.DetailPresenter;
import eu.antoniolopez.themoviedb.ui.adapter.SimilarMoviesAdapter;
import eu.antoniolopez.themoviedb.ui.contract.MovieDetailContract;
import eu.antoniolopez.themoviedb.utils.SnackbarUtils;
import eu.antoniolopez.themoviedb.utils.TextUtils;
import eu.antoniolopez.themoviedb.vo.Movie;
import eu.antoniolopez.themoviedb.vo.MovieError;

public class DetailActivity extends BaseActivity implements MovieDetailContract, SimilarMoviesAdapter.OnFinishedListener {

    private static final String MOVIE = "movie";

    @BindView(R.id.viewpager_container) ViewPager mViewPager;

    @Inject SimilarMoviesAdapter similarMoviesAdapter;
    @Inject DetailPresenter presenter;
    private State state;

    public static Intent newInstance(Context context, Movie movie) {
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(MOVIE, movie);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        supportPostponeEnterTransition();
        ButterKnife.bind(this);

        initState();

        if (savedInstanceState != null) {
            similarMoviesAdapter.onRestoreInstanceState(savedInstanceState);
        } else {
            Bundle extras = getIntent().getExtras();
            Movie movie = extras.getParcelable(MOVIE);
            presenter.onMovieReceived(movie.getId());
            similarMoviesAdapter.setMovie(movie);
            similarMoviesAdapter.setListener(this);
        }
        mViewPager.setAdapter(similarMoviesAdapter);
        mViewPager.setOnPageChangeListener(similarMoviesAdapter);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        similarMoviesAdapter.onSaveInstanceState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroy();
        super.onDestroy();
    }

    @Override
    public void showSimilarMovies(List<Movie> movieList) {
        similarMoviesAdapter.setList(movieList);
    }

    @Override
    public void showError(MovieError movieError) {
        int errorId = 0;
        switch (movieError.getRequestError()) {
            case NO_DATA:
                errorId = R.string.error_data;
                break;
            case NO_CONNECTION:
                errorId = R.string.error_data_connection;
                break;
            case REQUEST_ERROR:
                errorId = R.string.error_response;
                break;
        }
        if (errorId > 0) {
            String text;
            if (!TextUtils.isEmpty(movieError.getStatusMessage())) {
                text = getString(R.string.composed_error, getString(errorId), movieError.getStatusMessage());
            } else {
                text = getString(errorId);
            }
            SnackbarUtils.showLongSnackBar(findViewById(android.R.id.content), text, Snackbar.LENGTH_LONG);
        }
    }

    @Override
    public void close() {
        finish();
    }

    @Override
    public void onScrollFinished() {
        presenter.onFinished();
    }

    @Override
    public void onBackPressed() {
        if (getResources().getConfiguration().orientation != state.originalOrientation) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    //TODO change this solution, save the orientation on first open to disable animatio if orientation changes
    private void initState() {
        String tag = "state";
        FragmentManager fm = getSupportFragmentManager();
        this.state = (State) fm.findFragmentByTag(tag);
        if (this.state == null) {
            this.state = new State();
            fm.beginTransaction().add(this.state, tag).commit();
            // store original screen orientation
            this.state.originalOrientation = getResources().getConfiguration().orientation;
        }
    }

    public static class State extends Fragment {
        private int originalOrientation;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }
    }
}
