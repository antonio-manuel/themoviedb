package eu.antoniolopez.themoviedb.ui.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import eu.antoniolopez.themoviedb.R;
import eu.antoniolopez.themoviedb.network.factory.UrlFactory;
import eu.antoniolopez.themoviedb.vo.Movie;

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.MovieViewHolder> {

    public interface OnItemClickListener {
        void onMovieItemClicked(Movie movie, ImageView image);
    }

    private ArrayList<Movie> movieList;
    private final LayoutInflater layoutInflater;

    private OnItemClickListener onItemClickListener;

    @Inject
    MovieListAdapter(Context context) {
        this.layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.movieList = new ArrayList<>();
    }

    @Override
    public int getItemCount() {
        return (this.movieList != null) ? this.movieList.size() : 0;
    }

    @Override
    public MovieViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View view = this.layoutInflater.inflate(R.layout.list_item_movie, parent, false);
        return new MovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MovieViewHolder holder, final int position) {
        final Movie movie = this.movieList.get(position);
        holder.text.setText(movie.getOriginalName());
        holder.view.setContentDescription(movie.getOriginalName());
        holder.itemView.setOnClickListener(v -> {
            if (MovieListAdapter.this.onItemClickListener != null) {
                MovieListAdapter.this.onItemClickListener.onMovieItemClicked(movie, holder.image);
            }
        });

        ViewCompat.setTransitionName(holder.image, String.valueOf(movie.getId()));

        holder.image.setImageResource(R.drawable.ic_photo_black_48dp);
        holder.image.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        String url = UrlFactory.getPosterUrl(movie.getPosterPath());
        if (url != null) {
            Glide.with(holder.image.getContext()).load(url).into(new DrawableImageViewTarget(holder.image) {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                    super.onResourceReady(resource, transition);
                    view.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }
            });
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setMovieList(ArrayList<Movie> movieList) {
        this.validateUsersCollection(movieList);
        this.movieList = movieList;
        this.notifyDataSetChanged();
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    private void validateUsersCollection(List<Movie> movieList) {
        if (movieList == null) {
            throw new IllegalArgumentException("The list cannot be null");
        }
    }

    static class MovieViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_icon) ImageView image;
        @BindView(R.id.item_text) TextView text;
        @BindView(R.id.container) View view;

        MovieViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}