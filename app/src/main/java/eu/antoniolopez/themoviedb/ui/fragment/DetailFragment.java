package eu.antoniolopez.themoviedb.ui.fragment;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.DrawableImageViewTarget;
import com.bumptech.glide.request.transition.Transition;

import java.text.DateFormat;

import eu.antoniolopez.themoviedb.R;
import eu.antoniolopez.themoviedb.network.factory.UrlFactory;
import eu.antoniolopez.themoviedb.utils.ImageUtils;
import eu.antoniolopez.themoviedb.vo.Movie;

public class DetailFragment extends BaseFragment {

    private Movie movie;

    private View rootView;
    private String imageName;

    public static Fragment newInstance(Movie movie) {
        DetailFragment detail = new DetailFragment();
        detail.setMovie(movie);
        detail.setRetainInstance(true);
        return detail;
    }

    public static Fragment newInstance(Movie movie, String imageName) {
        DetailFragment detail = new DetailFragment();
        detail.setMovie(movie);
        detail.setImageName(imageName);
        detail.setRetainInstance(true);
        return detail;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        rootView = inflater.inflate(R.layout.fragment_detail, container, false);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        initToolbar();
        cleanData();
        drawDetail();
    }

    private void initToolbar(){
        Toolbar toolbar = rootView.findViewById(R.id.detail_toolbar);
        AppCompatActivity appCombatActivity = (AppCompatActivity) getActivity();
        appCombatActivity.setSupportActionBar(toolbar);
        appCombatActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        appCombatActivity.getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    private void cleanData() {
        ((ImageView) rootView.findViewById(R.id.backdrop_image)).setImageResource(android.R.color.darker_gray);
        ((ImageView) rootView.findViewById(R.id.detail_image)).setImageResource(android.R.color.darker_gray);
    }

    private void drawDetail() {
        CollapsingToolbarLayout ctlLayout = rootView.findViewById(R.id.ctlLayout);
        String originalTitle;
        if (movie.getPosterPath() != null && movie.getPosterPath().trim().length() > 0) {
            originalTitle = movie.getOriginalName();
        } else {
            originalTitle = getString(R.string.detail_notitle);
        }
        ctlLayout.setTitle(movie.getOriginalName());

        loadImages();

        ((TextView) rootView.findViewById(R.id.detail_title)).setText(originalTitle);
        DateFormat dateFormat = android.text.format.DateFormat.getMediumDateFormat(getActivity());
        String date = dateFormat.format(movie.getFirstAirDate());
        ((TextView) rootView.findViewById(R.id.detail_release_date)).setText(getString(R.string.detail_releasedate, date));
        if (movie.getOverview() != null && movie.getOverview().trim().length() > 0) {
            ((TextView) rootView.findViewById(R.id.detail_overview)).setText(movie.getOverview());
        } else {
            ((TextView) rootView.findViewById(R.id.detail_overview)).setText(getString(R.string.detail_nooverview));
        }

        double rate = movie.getVoteAverage();
        setStar(rootView, R.id.star01, rate);
        rate -= 2;
        setStar(rootView, R.id.star02, rate);
        rate -= 2;
        setStar(rootView, R.id.star03, rate);
        rate -= 2;
        setStar(rootView, R.id.star04, rate);
        rate -= 2;
        setStar(rootView, R.id.star05, rate);
    }

    private void loadImages() {
        final ImageView imgBackdrop = rootView.findViewById(R.id.backdrop_image);
        final ImageView imgPoster = rootView.findViewById(R.id.detail_image);

        imgBackdrop.setImageResource(R.drawable.ic_photo_black_48dp);
        imgBackdrop.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        String url = UrlFactory.getBackDropUrl(movie.getBackdropPath());
        if (url != null) {
            Glide.with(imgBackdrop.getContext()).load(url).into(new DrawableImageViewTarget(imgBackdrop) {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                    super.onResourceReady(resource, transition);
                    Window window = getActivity().getWindow();
                    CollapsingToolbarLayout ctlLayout = rootView.findViewById(R.id.ctlLayout);
                    Palette p = Palette.from(ImageUtils.drawableToBitmap(resource)).generate();
                    int color = p.getVibrantColor(getResources().getColor(R.color.colorPrimary));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
                        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
                        window.setStatusBarColor(p.getDarkVibrantColor(getResources().getColor(R.color.colorPrimaryDark)));
                    }
                    ctlLayout.setContentScrimColor(color);
                    ctlLayout.setStatusBarScrimColor(color);
                    view.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }
            });
        }

        imgPoster.setImageResource(R.drawable.ic_photo_black_48dp);
        imgPoster.setScaleType(ImageView.ScaleType.CENTER_INSIDE);

        url = UrlFactory.getPosterUrl(movie.getPosterPath());
        if (url != null) {
            Glide.with(imgPoster.getContext()).load(url).into(new DrawableImageViewTarget(imgPoster) {
                @Override
                public void onResourceReady(@NonNull Drawable resource, @Nullable Transition<? super Drawable> transition) {
                    super.onResourceReady(resource, transition);
                    getActivity().supportStartPostponedEnterTransition();
                    view.setScaleType(ImageView.ScaleType.CENTER_CROP);
                }
            });
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            imgPoster.setTransitionName(imageName);
        }
    }

    private void setStar(View rootView, int id, double value) {
        if (value > 1.5) {
            ((ImageView) rootView.findViewById(id)).setImageResource(R.drawable.ic_star_24dp);
        } else {
            if (value > 0.5) {
                ((ImageView) rootView.findViewById(id)).setImageResource(R.drawable.ic_star_half_24dp);
            } else {
                ((ImageView) rootView.findViewById(id)).setImageResource(R.drawable.ic_star_border_24dp);
            }
        }
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }
}
