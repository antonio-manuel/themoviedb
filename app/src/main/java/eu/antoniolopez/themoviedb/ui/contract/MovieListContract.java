package eu.antoniolopez.themoviedb.ui.contract;

import java.util.List;

import eu.antoniolopez.themoviedb.network.RequestError;
import eu.antoniolopez.themoviedb.vo.Movie;
import eu.antoniolopez.themoviedb.vo.MovieError;

public interface MovieListContract {

    void showLoading();

    void hideLoading();

    void updateData(List<Movie> movieList, int page, int totalPages);

    void showError(MovieError movieError);

}
