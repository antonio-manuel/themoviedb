package eu.antoniolopez.themoviedb.usecase;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import javax.inject.Inject;

import io.reactivex.Observable;

public class CheckConnectivity {

    private final Context context;

    @Inject
    public CheckConnectivity(Context context) {
        this.context = context;
    }

    public Observable<Boolean> isOnline() {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return Observable.just(netInfo != null && netInfo.isConnected());
    }
}
