package eu.antoniolopez.themoviedb.usecase;

import javax.inject.Inject;

import eu.antoniolopez.themoviedb.di.qualifier.ForObserveScheduler;
import eu.antoniolopez.themoviedb.di.qualifier.ForSubscribeScheduler;
import eu.antoniolopez.themoviedb.network.mapper.MoviesPageMapper;
import eu.antoniolopez.themoviedb.network.request.MovieRequest;
import eu.antoniolopez.themoviedb.vo.MoviesPage;
import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class GetSimilarMovies {

    private final MovieRequest movieRequest;
    private final MoviesPageMapper mapper;
    private final Scheduler subscribeScheduler;
    private final Scheduler observeScheduler;

    @Inject
    public GetSimilarMovies(MovieRequest movieRequest, MoviesPageMapper mapper, @ForSubscribeScheduler Scheduler subscribeScheduler, @ForObserveScheduler Scheduler observeScheduler) {
        this.movieRequest = movieRequest;
        this.mapper = mapper;
        this.subscribeScheduler = subscribeScheduler;
        this.observeScheduler = observeScheduler;
    }

    public Observable<MoviesPage> getSimilarMovies(String id) {
        return
        movieRequest.getSimilar(id)
                .map(moviesPageDto -> mapper.transform(moviesPageDto))
                .subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler);
    }
}
