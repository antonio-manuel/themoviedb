package eu.antoniolopez.themoviedb.usecase;

import javax.inject.Inject;

import eu.antoniolopez.themoviedb.di.qualifier.ForObserveScheduler;
import eu.antoniolopez.themoviedb.di.qualifier.ForSubscribeScheduler;
import eu.antoniolopez.themoviedb.network.mapper.MoviesPageMapper;
import eu.antoniolopez.themoviedb.network.request.MovieRequest;
import eu.antoniolopez.themoviedb.vo.MoviesPage;
import io.reactivex.Observable;
import io.reactivex.Scheduler;

public class GetPopularMovies {

    private final MovieRequest movieRequest;
    private final MoviesPageMapper mapper;
    private final Scheduler subscribeScheduler;
    private final Scheduler observeScheduler;

    @Inject
    public GetPopularMovies(MovieRequest movieRequest, MoviesPageMapper mapper, @ForSubscribeScheduler Scheduler subscribeScheduler, @ForObserveScheduler Scheduler observeScheduler) {
        this.movieRequest = movieRequest;
        this.mapper = mapper;
        this.subscribeScheduler = subscribeScheduler;
        this.observeScheduler = observeScheduler;
    }

    public Observable<MoviesPage> getPage(int page) {
        return movieRequest.getPage(page)
                .map(moviesPageDto -> mapper.transform(moviesPageDto))
                .subscribeOn(subscribeScheduler)
                .observeOn(observeScheduler);
    }
}
