package eu.antoniolopez.themoviedb.network.request;

import eu.antoniolopez.datamodel.MoviesPageDto;
import eu.antoniolopez.themoviedb.network.service.MovieApiService;
import eu.antoniolopez.testingutils.data.DtoProvider;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.annotations.NonNull;

public class DebugMovieRequestImpl extends MovieRequest {

    private final MovieApiService movieApiService;
    private static boolean isServiceEnabled = true;

    public DebugMovieRequestImpl(MovieApiService movieApiService) {
        this.movieApiService = movieApiService;
    }

    @Override
    public Observable<MoviesPageDto> getPage(int page){
        if(isServiceEnabled) {
            return movieApiService.getPopularMovies(page);
        }else {
            return Observable.create(new ObservableOnSubscribe<MoviesPageDto>() {
                @Override
                public void subscribe(@NonNull ObservableEmitter<MoviesPageDto> observableEmitter) throws Exception {
                    MoviesPageDto moviesPageDto = new MoviesPageDto();
                    moviesPageDto.setPage(1);
                    moviesPageDto.setTotalPages(2);
                    moviesPageDto.setMovies(DtoProvider.getListOfMovies());
                    observableEmitter.onNext(moviesPageDto);
                    observableEmitter.onComplete();
                }
            });
        }
    }

    @Override
    public Observable<MoviesPageDto> getSimilar(String id){
        if(isServiceEnabled) {
            return movieApiService.getSimilarMovies(id);
        }else{
            return Observable.create(new ObservableOnSubscribe<MoviesPageDto>() {
                @Override
                public void subscribe(@NonNull ObservableEmitter<MoviesPageDto> observableEmitter) throws Exception {
                    MoviesPageDto moviesPageDto = new MoviesPageDto();
                    moviesPageDto.setPage(1);
                    moviesPageDto.setTotalPages(2);
                    moviesPageDto.setMovies(DtoProvider.getListOfMovies());
                    observableEmitter.onNext(moviesPageDto);
                    observableEmitter.onComplete();
                }
            });
        }
    }

    public static void setServiceEnabled(boolean enabled) {
        isServiceEnabled = enabled;
    }
}
