package eu.antoniolopez.themoviedb.di;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import eu.antoniolopez.themoviedb.network.request.MovieRequest;
import eu.antoniolopez.themoviedb.network.request.DebugMovieRequestImpl;
import eu.antoniolopez.themoviedb.network.service.MovieApiService;

@Module
public class RequestModule {

    @Provides
    @Singleton
    public MovieRequest provideMovieRequest(MovieApiService service) {
        return new DebugMovieRequestImpl(service);
    }

}
