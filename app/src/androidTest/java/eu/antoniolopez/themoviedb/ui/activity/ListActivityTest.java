package eu.antoniolopez.themoviedb.ui.activity;

import android.support.test.espresso.NoMatchingViewException;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;

import com.schibsted.spain.barista.rule.BaristaRule;
import com.schibsted.spain.barista.rule.flaky.Repeat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import eu.antoniolopez.datamodel.MovieDto;
import eu.antoniolopez.testingutils.data.DtoProvider;
import eu.antoniolopez.testingutils.ui.RecyclerViewInteraction;
import eu.antoniolopez.themoviedb.R;
import eu.antoniolopez.themoviedb.network.mapper.MoviesMapper;
import eu.antoniolopez.themoviedb.network.request.DebugMovieRequestImpl;
import eu.antoniolopez.themoviedb.vo.Movie;

import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.hasDescendant;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.schibsted.spain.barista.assertion.BaristaRecyclerViewAssertions.assertRecyclerViewItemCount;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ListActivityTest {

    @Rule
    public BaristaRule<ListActivity> activityRule = BaristaRule.create(ListActivity.class);

    @Test
    @Repeat(times = 1)
    public void checkViewBindFullList() {
        DebugMovieRequestImpl.setServiceEnabled(false);

        startActivity();

        assertRecyclerContainsAllCollectionItems(DtoProvider.getListOfMovies());
    }

    private void assertRecyclerContainsAllCollectionItems(List<MovieDto> movieDtoList) {
        MoviesMapper mapper = new MoviesMapper();
        List<Movie> movieList = mapper.transform(movieDtoList);

        assertRecyclerViewItemCount(R.id.list_container, movieList.size());

        RecyclerViewInteraction.<Movie>onRecyclerView(withId(R.id.list_container))
                .withItems(movieList)
                .check(new RecyclerViewInteraction.ItemViewAssertion<Movie>() {
                    @Override
                    public void check(Movie movie, View view, NoMatchingViewException e) {
                        matches(hasDescendant(withText(movie.getOriginalName()))).check(view, e);
                    }
                });

    }

    private void startActivity() {
        activityRule.launchActivity();
    }
}
