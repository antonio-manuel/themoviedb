package eu.antoniolopez.themoviedb.ui.activity;

import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.schibsted.spain.barista.rule.BaristaRule;
import com.schibsted.spain.barista.rule.flaky.Repeat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Date;
import java.util.List;

import eu.antoniolopez.datamodel.MovieDto;
import eu.antoniolopez.testingutils.data.DtoProvider;
import eu.antoniolopez.themoviedb.R;
import eu.antoniolopez.themoviedb.network.request.DebugMovieRequestImpl;
import eu.antoniolopez.themoviedb.vo.Movie;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.swipeLeft;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions.assertDisplayed;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class DetailActivityTest {

    @Rule
    public BaristaRule<DetailActivity> activityRule = BaristaRule.create(DetailActivity.class);

    @Test
    @Repeat(times = 1)
    public void checkClickedMovieIsDisplayed() {
        DebugMovieRequestImpl.setServiceEnabled(false);
        Movie movie = getMovie();

        startActivity(movie);

        assertDisplayed(movie.getOriginalName());
        assertDisplayed(movie.getOverview());
    }

    @Test
    @Repeat(times = 1)
    public void checkViewPagerLoadAllSimilarMovies() {
        DebugMovieRequestImpl.setServiceEnabled(false);
        Movie movie = getMovie();
        List<MovieDto> dtoList = DtoProvider.getListOfMovies();

        startActivity(movie);

        for(MovieDto movieDto : dtoList) {
            onView(withId(R.id.viewpager_container)).perform(swipeLeft());
            assertDisplayed(movieDto.getOriginalName());
            assertDisplayed(movieDto.getOverview());
        }
    }

    private void startActivity(Movie movie) {
        Intent intent = DetailActivity.newInstance(InstrumentationRegistry.getTargetContext(), movie);
        activityRule.launchActivity(intent);
    }


    public Movie getMovie() {
        Movie movie = new Movie();
        movie.setId(1);
        movie.setPosterPath("/jIhL6mlT7AblhbHJgEoiBIOUVl1.jpg");
        movie.setBackdropPath("mUkuc2wyV9dHLG0D0Loaw5pO2s8.jpg");
        movie.setVoteAverage(5.5);
        movie.setOverview("Seven noble families fight for control of the mythical land of Westeros.");
        movie.setFirstAirDate(new Date());
        movie.setOriginalLanguage("es");
        movie.setOriginalName("Original name");
        return movie;
    }
}